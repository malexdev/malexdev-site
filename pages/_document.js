import Document, { Head, Main, NextScript } from 'next/document'
import { Flex, Box } from 'reflexbox'

export default class extends Document {
  render () {
    return (
      <html>

        <Head>
          <meta name='viewport' content='initial-scale=1.0, width=device-width' />
          <link href='https://unpkg.com/normalize.css@^4.1.1' rel='stylesheet' />
          <link href='https://unpkg.com/@blueprintjs/core@^1.15.0/dist/blueprint.css' rel='stylesheet' />
          <link href='/static/css/malexdev.css' rel='stylesheet' />
        </Head>

        <body>
          <Flex align='center'>
            <Box auto p={3}>
              <Main />
            </Box>
          </Flex>
          <NextScript />
        </body>

      </html>
    )
  }
}
