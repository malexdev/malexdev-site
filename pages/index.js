import * as Blueprint from '@blueprintjs/core'
import { Flex, Box } from 'reflexbox'
import Head from 'next/head'

export default props => (
  <div>
    <Head>
      <title>malexdev::home</title>
    </Head>

    <UnderConstruction />
  </div>
)

const UnderConstruction = () => (
  <Flex align='center'>
    <Box auto p={1}>
      <div className='pt-callout pt-intent-warning pt-icon-git-new-branch'>
        <h5><b>//</b> construction</h5>
        <b>malexdev.net</b> is currently under construction. Thanks for your patience!
      </div>
    </Box>
  </Flex>
)